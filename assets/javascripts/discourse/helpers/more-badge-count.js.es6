import { htmlHelper } from 'discourse/lib/helpers';


export default htmlHelper((totalBadges, featuredBadges) => {
  if (!totalBadges || totalBadges > 3) {
    let otherBadgeCount = totalBadges - featuredBadges;
    return `+ ${otherBadgeCount} MORE`;
  }
});
export default Ember.Component.extend({
  tagName: 'div',
  layoutName: 'components/custom-fields-data',
  classNameBindings: ['custom-fields'],
  actions: {
    triggerPrivateMessage(user) {
      this.sendAction('triggerPrivateMessage', user);
    }
  },

  custom_fields: function() {
    var custom_fields = Ember.Object.create({
      "real_state_brokerage": "",
      "location": ""
    });

    var username = this.get('user').username;

    $.ajax({
      url: '/custom_user_fields',
      data: { user_name: username }
    }).then(function(res) {
      var primary_brokerage = res.primary_brokerage;
      var state             = res.province_state;
      var city              = res.city;
      var email             = res.email;
      var facebook          = res.facebook;
      var twitter           = res.twitter;
      var instagram         = res.instagram;
      var linkedin          = res.linkedin;
      var youtube           = res.youtube;
      var loc               = new Array();

      function socialUrlSanitizer(input, network) {
         if (!/^(f|ht)tps?:\/\//i.test(input) && /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(input)) {
            return "http://" + input;
         } else if (/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/.test(input)) {
            return input;
         } else {
            if (network === 'linkedin')
              return `http://${network}.com/in/${input}`;
            else {
              return `http://${network}.com/${input}`;
            }
         }
      }

      if(facebook) {
        custom_fields.set("facebook_url", socialUrlSanitizer(facebook, 'facebook'))
      }

      if(twitter) {
        custom_fields.set("twitter_url", socialUrlSanitizer(twitter, 'twitter'))
      }

      if(instagram) {
        custom_fields.set("instagram_url", socialUrlSanitizer(instagram, 'instagram'))
      }

      if(linkedin) {
        custom_fields.set("linkedin_url", socialUrlSanitizer(linkedin, 'linkedin'))
      }

      if(youtube) {
        custom_fields.set("youtube_url", socialUrlSanitizer(youtube, 'youtube'))
      }

      if(email) {
        custom_fields.set("email", email)
      }

      if(primary_brokerage) {
        var regex = new RegExp(', [A-z .0-9]+, [A-Z]{2}$');
        primary_brokerage = primary_brokerage.replace(regex, '');

        custom_fields.set("primary_brokerage", primary_brokerage);
      } else {
        custom_fields.set("primary_brokerage", "");
      }

      if(city) {
        loc.push(city)
      }

      if(state) {
        loc.push(state)
      }
      custom_fields.set("location", loc.join(", "));
    });

    return custom_fields;
  }.property('custom_fields')
});
import { withPluginApi } from 'discourse/lib/plugin-api';

export default {
  name: "alt-profile-initializer",

  initialize(container) {

    //plugin is compatible with v 0.1 of API
    withPluginApi('0.1', api => {

      const UserBadgesRoute = container.lookupFactory('route:user-badges');
      UserBadgesRoute.reopen({
        model() {
          this.controllerFor("user").set("forceExpand", true);
          return this._super();
        }
      });

      const UserSummaryRoute = container.lookupFactory('route:user-summary');
      UserSummaryRoute.reopen({
        model() {
          this.controllerFor("user").set("forceExpand", true);
          this.controllerFor("application").set("showFooter", true);
          return this._super();
        }
      });

      const UserRoute = container.lookupFactory('route:user');
      UserRoute.reopen({
        beforeModel: function() {
          if (this.site.mobileView) {
            this.replaceWith('user.summary');
          } else {
            this.transitionTo('user.summary');
          }
          return this._super();
        }
      });      

    });

  }
}